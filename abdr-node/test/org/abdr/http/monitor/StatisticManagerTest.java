package org.abdr.http.monitor;

import java.util.ArrayList;
import java.util.List;

import org.abdr.configuration.Configuration;
import org.abdr.http.monitor.algo.BestNodeAlgorithm;
import org.abdr.node.Node;
import org.abdr.node.NodeFactory;
import org.eclipse.jetty.util.log.Log;
import org.junit.Test;

public class StatisticManagerTest {

  @Test
  public final void testComputeBestPosition() throws Exception {
    NodeFactory factory = new NodeFactory();
    List<Node> nodes = new ArrayList<>();
    nodes.add(factory.createRemoteNode("localhost", 8081));
    nodes.add(factory.createRemoteNode("localhost", 8082));
    StatisticManager manager = new StatisticManager(new Configuration("config/monitor.properties"));
    manager.refresh();
    List<Node> n = new BestNodeAlgorithm().computeBestNodes(manager, nodes);
    Log.info(manager.toString());
    Log.info("Elected node " + n.get(0));
  }
}
