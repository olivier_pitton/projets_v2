package org.abdr.http.slave;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.abdr.http.HttpClient;
import org.abdr.http.response.ReadResponse;
import org.abdr.http.response.Response;
import org.abdr.node.Node;
import org.abdr.node.NodeFactory;
import org.abdr.transaction.ReadTransaction;
import org.abdr.transaction.SerializableKey;
import org.abdr.transaction.impl.MigrationTransaction;
import org.abdr.transaction.impl.StandardWriteTransaction;
import org.junit.Before;
import org.junit.Test;

public class MigrationTest {

	private List<SerializableKey> keys = keys(Arrays.asList("P1", "P2", "P3"));
	private Node node = new NodeFactory().createRemoteNode("localhost", 8081);

	@Before
	public void writeData() throws IOException, ClassNotFoundException {
		Map<SerializableKey, byte[]> val = new HashMap<>(keys.size());
		for (SerializableKey k : keys) {
			val.put(k, k.toString().getBytes());
		}
		try (HttpClient httpClient = new HttpClient()) {
			Response code = httpClient.writeTransaction(new StandardWriteTransaction(keys, val), node);
			assertEquals(200, code.getStatus());
		}
	}

	@Test
	public void migrationTransactionTest() throws Exception {
		try (HttpClient httpClient = new HttpClient()) {
			httpClient.writeTransaction(new MigrationTransaction(keys, "localhost", 8082), node);
			ReadResponse readTransactionFirst = httpClient.readTransaction(new ReadTransaction(keys), node);
			assertNotNull(readTransactionFirst);
			Map<SerializableKey, byte[]> firstSearch = readTransactionFirst.getSearch();
			assertNotNull(firstSearch);
			assertEquals(0, firstSearch.size());
			ReadResponse readTransaction = httpClient.readTransaction(new ReadTransaction(keys), new NodeFactory().createRemoteNode("localhost", 8082));
			assertNotNull(readTransaction);
			Map<SerializableKey, byte[]> secondSearch = readTransaction.getSearch();
			assertNotNull(secondSearch);
			assertEquals(3, secondSearch.size());
			for (SerializableKey key : keys) {
				assertEquals(new String(secondSearch.get(key)), key.toString());
			}
		}
	}

	private List<SerializableKey> keys(List<String> keys) {
		List<SerializableKey> res = new ArrayList<>(keys.size());
		for (String key : keys) {
			res.add(new SerializableKey(key, key));
		}
		return res;
	}
}
