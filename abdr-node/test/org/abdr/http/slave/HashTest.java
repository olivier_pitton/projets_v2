package org.abdr.http.slave;

import org.abdr.transaction.SerializableKey;
import org.junit.Test;

public class HashTest {

	@Test
	public void testHash() {
		for(int i = 0 ; i < 1000 ; i++) {
			int hashCode = new SerializableKey("RDFSffdf" + i, "fdsf" + i).hashCode();
			long res = (long) (Math.abs((long) hashCode) * Math.random() % 2L);
			System.out.println(res);
		}
	}
	
}
