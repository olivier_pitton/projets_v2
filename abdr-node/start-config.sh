#!/bin/bash

export KVSTORE_PATH=$1
rm -rf ${KVSTORE_PATH}
java -jar lib/kvstore.jar kvlite -root ${KVSTORE_PATH} -port $2
