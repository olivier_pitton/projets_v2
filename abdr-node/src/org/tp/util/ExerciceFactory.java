package org.tp.util;

import oracle.kv.KVStore;

import org.tp.exercice1.A1;
import org.tp.exercice1.A2;
import org.tp.exercice2.M1;
import org.tp.exercice2.M2;

/**
 * Une factory pour créer l'ensemble des exercices, qui ont la même interface.
 * 
 * @author pitton
 * 
 */
public class ExerciceFactory {

	public ExerciceFactory() {
	}

	/**
	 * Retourne une instance de {@link A1}
	 * 
	 * @param store
	 *          une instance de KVStore
	 * @return une instance de {@link A1}
	 */
	public Exercice createA1(KVStore store) {
		return new A1(store);
	}

	/**
	 * Retourne une instance de {@link A2}
	 * 
	 * @param store
	 *          une instance de KVStore
	 * @return une instance de {@link A2}
	 */
	public Exercice createA2(KVStore store) {
		return new A2(store);
	}

	/**
	 * Retourne une instance de {@link M1}
	 * 
	 * @param store
	 *          une instance de KVStore
	 * @return une instance de {@link M1}
	 */
	public Exercice createM1(KVStore store) {
		return new M1(store);
	}

	/**
	 * Retourne une instance de {@link M2}
	 * 
	 * @param store
	 *          une instance de KVStore
	 * @return une instance de {@link M2}
	 */
	public Exercice createM2(KVStore store) {
		return new M2(store);
	}

}
