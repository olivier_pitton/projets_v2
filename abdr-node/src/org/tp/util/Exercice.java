package org.tp.util;


import java.io.Closeable;

/**
 * L'interface Exercice représente toutes les opérations à réaliser pour chaque
 * question du TME KVStore
 * @author pitton
 *
 */
public interface Exercice extends Closeable {

	/**
	 * Exécute l'ensemble des opérations à effectuer
	 * @return l'instance courante
	 * @throws Exception 
	 */
  Exercice go() throws Exception;
  
  /**
   * Peuple la base de données si besoin.
   * @return
   */
  Exercice populate();
  
  /**
   * Retourne vraie si la base de données a été remplie par l'appel à 
   * {@link #populate()}
   * @return
   */
  boolean isPopulate();
  
  /**
   * Met à jour le fait que la base de données a été remplie par l'appel à 
   * {@link #populate()}
   * @param populate
   */
  void setPopulate(boolean populate);
}
