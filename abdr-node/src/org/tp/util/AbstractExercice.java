package org.tp.util;

import oracle.kv.KVStore;

/**
 * Classe abstraite permettant de factoriser le code de tous les exercices.
 * @author pitton
 *
 */
public abstract class AbstractExercice implements Exercice {

	// L'instance courante de KVStore
  protected final KVStore store;
  // Savoir si la base a été remplie
  private boolean populate = false;

  public AbstractExercice(KVStore store) {
    this(store, false);
  }

  public AbstractExercice(KVStore store, boolean populate) {
    this.store = store;
    this.populate = populate;
  }

  @Override
  public boolean isPopulate() {
    return populate;
  }

  @Override
  public void setPopulate(boolean populate) {
    this.populate = populate;
  }

  @Override
  public Exercice populate() {
  	// A redéfinir pour les exercices ayant besoin de données
  	// avant leur lancement
    return this;
  }

  @Override
  public void close() {
    store.close();
  }
}
