package org.tp.exercice1;

import java.util.UUID;

import oracle.kv.KVStore;
import oracle.kv.Key;
import oracle.kv.Value;
import oracle.kv.Version;

import org.tp.util.AbstractExercice;
import org.tp.util.Exercice;

/**
 * Implémentation de l'exercice 1 A2
 * @author pitton
 *
 */
public class A2 extends AbstractExercice {

  public A2(KVStore store) {
    super(store);
  }

  @Override
  public Exercice go() throws Exception {
  	// On crée la clé
    Key key = Key.createKey("P1");
    for (int i = 0; i < 1000; i++) {
    	// On crée la valeur associée
      Value value = Value.createValue(Integer.toString(i + 1).getBytes());
      // On crée une version
      Version version = new Version(UUID.randomUUID(), i);
      // On insère
      store.putIfVersion(key, value, version);
    }
    System.out.println(new String(store.get(key).getValue().getValue()));
    return this;
  }

}