package org.tp.exercice1;


import oracle.kv.KVStore;
import oracle.kv.Key;
import oracle.kv.Value;

import org.tp.util.AbstractExercice;
import org.tp.util.Exercice;

/**
 * Implémentation de l'exercice 1 A1
 * @author pitton
 *
 */
public class A1 extends AbstractExercice{

   public A1(KVStore store) {
    super(store);
  }

  @Override
  public Exercice go() throws Exception {
  	// Création de la clé P1
    Key key = Key.createKey("P1");
    // On effectue 1000 insertions directes dans l'intervalle [1;1000]
    for(int i = 0 ; i < 1000 ; i++) {
      store.put(key, Value.createValue(Integer.toString(i + 1).getBytes()));
    }
    // On affiche la valeur insérée (1000)
    System.out.println(new String(store.get(key).getValue().getValue()));
    return this;
  }

}
