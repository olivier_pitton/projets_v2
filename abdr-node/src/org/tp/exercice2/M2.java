package org.tp.exercice2;

import java.util.ArrayList;
import java.util.List;

import oracle.kv.KVStore;
import oracle.kv.Key;
import oracle.kv.Value;

import org.tp.util.AbstractExercice;
import org.tp.util.Exercice;

/**
 * Implémentation du dernier exercice M2
 * @author pitton
 *
 */

public class M2 extends AbstractExercice {

  private List<Key> keys;

  public M2(KVStore store) {
    super(store, false);
  }

  @Override
  public Exercice populate() {
  	// On remplit la base de données
    keys = new ArrayList<>(10);
    for (int i = 1; i <= 10; i++) {
      keys.add(Key.createKey("C1", "P" + i));
    }
    if (isPopulate()) {
      System.out.println("Réinitialisation des données...");
      Value value = Value.createValue("0".getBytes());
      for (int i = 0; i < 10; i++) {        
        store.put(keys.get(i), value);
      }
    }
    // On affiche les résultats obtenus avant les transactions
    System.out.println("Avant les transactions");
    for (int i = 0; i < 5; i++) {
      System.out.println(new String(store.get(keys.get(i)).getValue().getValue()));
    }
    return super.populate();
  }

  @Override
  public Exercice go() throws Exception {
    System.out.println("Démarrage des transactions");
    for (int i = 0; i < 1000; i++) {
    	// On lance et exécute 1000 transactions
      new Transaction(store).commit(keys);
    }
    System.out.println("Fin des transactions");
    // On affiche le résultat
    for (int i = 0; i < 5; i++) {
      System.out.println(new String(store.get(keys.get(i)).getValue().getValue()));
    }
    return this;
  }
}
