package org.tp.exercice2;

import java.util.ArrayList;
import java.util.List;

import oracle.kv.KVStore;
import oracle.kv.Key;
import oracle.kv.Operation;
import oracle.kv.OperationExecutionException;
import oracle.kv.OperationFactory;
import oracle.kv.ReturnValueVersion.Choice;
import oracle.kv.Value;
import oracle.kv.ValueVersion;
import oracle.kv.Version;

/**
 * Implémentation d'une transaction. Cette classe permet de résoudre le problème
 * de cohérence de données lorsque plusieurs clients tentent de modifier les mêmes valeurs.
 * Si l'on exécute 3 transactions en parallèle, le résultat final sera bien 3000 pour toutes 
 * les valeurs.
 * @author pitton
 *
 */
public class Transaction {

  private final KVStore store;
  
  public Transaction(KVStore store) {
    this.store = store;
  }

  public void commit(List<Key> keyList) {
    OperationFactory factory = store.getOperationFactory();
    List<Operation> operations = new ArrayList<>(5);
    List<Version> versions = new ArrayList<>(5);
		// On récupère la valeur maximale parmi les 5 produits.
		// la liste de versions permet de retenir toutes les versions des 5 produits
		// elle est remplie par la méthode "getMax".
    Value val = getMax(keyList, versions);
    for (int i = 0; i < 5; i++) {
			// Pour les 5 clés, on insère le maximum, augmenté de 1, avec la version associée. 
      Operation operation = factory.createPutIfVersion(keyList.get(i), val, versions.get(i), Choice.NONE, true);
      operations.add(operation);
    }    
		try {
			// On exécute les 5 écritures.
			store.execute(operations);
		} catch (OperationExecutionException e) {
			// Si les écritures ont échoué, un autre client a modifié l'une des clés.
			// On recommence pour obtenir un nouveau maximum
			commit(keyList);
		}
  }
  

	/**
	 * Renvoie la valeur maximale parmi la liste de clés spécifiées. Le paramètre versions
	 * est rempli avec l'ensemble des versions de toutes les clés demandées.
	 */
  private Value getMax(List<Key> keys, List<Version> versions) {
  	int max = 0;
		for (int i = 0; i < 5; i++) {
			// On récupère la valeur associée au produit demandé
			ValueVersion valueVersion = store.get(keys.get(i));
			// On convertit la valeur en entier
			int subMax = Integer.parseInt(new String(valueVersion.getValue().getValue()));
			// On ajoute la version à la liste
			versions.add(valueVersion.getVersion());
			// Si la valeur récupérée est plus grande, elle est le maximum
			if (subMax >= max) {
				max = subMax;
			}
		}
		// On augmente le maximum de 1.
		max++;
		// On retourne la nouvelle valeur obtenue.
		return Value.createValue(Integer.toString(max).getBytes());
  }
}
