package org.abdr.tp;

import java.util.ArrayList;
import java.util.List;

import oracle.kv.KVStore;
import oracle.kv.Key;
import oracle.kv.Operation;
import oracle.kv.OperationExecutionException;
import oracle.kv.OperationFactory;
import oracle.kv.ReturnValueVersion.Choice;
import oracle.kv.Value;
import oracle.kv.ValueVersion;
import oracle.kv.Version;

import org.abdr.transaction.SerializableKey;
import org.abdr.transaction.WriteTransaction;

/**
 * La classe M2 est la représentation d'une transaction d'écriture dans notre solution. Nous avons adapté le TP afin 
 * que celui-ci puisse être exécuté sur notre plateforme. Le résultat produit est identique à ce qui a été vu en TP.
 * Cette classe contient la liste des clés que l'on souhaite modifier. Lors de l'exécution sur notre projet, il faut
 * donc spécifier les 5 clés des produits du TP.
 *
 */
public class M2 extends WriteTransaction {

	public M2() {
	}

	public M2(List<SerializableKey> keys) {
		super(keys);
	}

	@Override
	public void commit(KVStore store) throws Exception {
		// On récupère la liste des clés de la transaction d'écriture.
		// Cette liste de clés correspond aux clés que l'on souhaite modifier
		List<SerializableKey> keys = getKeys();
		// On convertit les "SerializableKey" en "Key" de KVStore car celles-ci
		// ne sont pas Serializable et ne peuvent donc être échangés sur le réseau.
		List<Key> keyList = new ArrayList<>(keys.size());
		for (SerializableKey k : keys) {
			keyList.add(k.getKey());
		}
		// On effectue le traitement
		commit(store, keyList);
	}

	private void commit(KVStore store, List<Key> keyList) {
		OperationFactory factory = store.getOperationFactory();
		List<Operation> operations = new ArrayList<>(5);
		List<Version> versions = new ArrayList<>(5);
		// On récupère la valeur maximale parmi les 5 produits.
		// la liste de versions permet de retenir toutes les versions des 5 produits
		// elle est remplie par la méthode "getMax".
		Value val = getMax(store, keyList, versions);
		for (int i = 0; i < 5; i++) {
			// Pour les 5 clés, on insère le maximum, augmenté de 1, avec la version associée. 
			Operation operation = factory.createPutIfVersion(keyList.get(i), val, versions.get(i), Choice.NONE, true);
			operations.add(operation);
		}
		try {
			// On exécute les 5 écritures.
			store.execute(operations);
		} catch (OperationExecutionException e) {
			// Si les écritures ont échoué, un autre client a modifié l'une des clés.
			// On recommence pour obtenir un nouveau maximum
			commit(store, keyList);
		}
	}

	/**
	 * Renvoie la valeur maximale parmi la liste de clés spécifiées. Le paramètre versions
	 * est rempli avec l'ensemble des versions de toutes les clés demandées.
	 */
	private Value getMax(KVStore store, List<Key> keys, List<Version> versions) {
		int max = 0;
		for (int i = 0; i < 5; i++) {
			// On récupère la valeur associée au produit demandé
			ValueVersion valueVersion = store.get(keys.get(i));
			// On convertit la valeur en entier
			int subMax = Integer.parseInt(new String(valueVersion.getValue().getValue()));
			// On ajoute la version à la liste
			versions.add(valueVersion.getVersion());
			// Si la valeur récupérée est plus grande, elle est le maximum
			if (subMax >= max) {
				max = subMax;
			}
		}
		// On augmente le maximum de 1.
		max++;
		// On retourne la nouvelle valeur obtenue.
		return Value.createValue(Integer.toString(max).getBytes());
	}

	@Override
	public void begin(KVStore store) throws Exception {
	}

	@Override
	public void abort(KVStore store, Exception ex) throws Exception {
		ex.printStackTrace();
	}

}
