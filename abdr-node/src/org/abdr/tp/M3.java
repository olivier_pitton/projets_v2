package org.abdr.tp;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import oracle.kv.KVStore;
import oracle.kv.Operation;
import oracle.kv.OperationExecutionException;
import oracle.kv.OperationFactory;
import oracle.kv.ReturnValueVersion.Choice;
import oracle.kv.Value;
import oracle.kv.ValueVersion;

import org.abdr.transaction.SerializableKey;
import org.abdr.transaction.impl.StandardWriteTransaction;

/**
 * La classe M3 est la représentation d'une transaction d'écriture dans notre solution. Elle
 * insère n (100 dans le TP) nouveaux objets. Ceux-ci sont contenus dans les variables d'instance
 * situées dans la classe mère.
 *
 */
public class M3 extends StandardWriteTransaction {

	public M3() {
	}

	public M3(List<SerializableKey> keys, Map<SerializableKey, byte[]> values) {
		super(keys, values);
	}

	@Override
	public void begin(KVStore store) throws Exception {
	}

	@Override
	public void commit(KVStore store) throws Exception {
		// On prépare l'exécution de la transaction
		OperationFactory factory = store.getOperationFactory();
		List<Operation> operations = new ArrayList<>(keys.size());
		for (int i = 0; i < keys.size(); i++) {
			// On récupère la clé à insérer
			SerializableKey serializableKey = keys.get(i);
			// On crée une nouvelle valeur, remplaçant la précédente avec les données publiées par le client
			Value newData = Value.createValue(getDatas().get(serializableKey)); 
			// On récupère la valeur associée, afin de la remplacer.
			ValueVersion val = store.get(serializableKey.getKey());
			Operation operation = null;
			// Si la valeur n'existait pas, alors il s'agit d'une simple insertion.
			if(val == null) {
				operation = factory.createPut(serializableKey.getKey(), newData, Choice.NONE, true);
			} else {
				// On crée l'opération. On associe la nouvelle valeur avec l'ancienne version de la donnée.
				operation = factory.createPutIfVersion(serializableKey.getKey(), newData, val.getVersion(), Choice.NONE, true);
			}
			operations.add(operation);
		}
		try {
			// On exécute l'ensemble des opérations
			store.execute(operations);
		} catch (OperationExecutionException e) {
			// Si échec, on recommence
			commit(store);
		}
	}

	@Override
	public void abort(KVStore store, Exception ex) throws Exception {
		ex.printStackTrace();
	}

}
