package org.abdr.tp.projet;

import java.io.Serializable;

public class Data implements Serializable {

	private String firstWord, secondWord, thirdWord, fourthWord, fifthWord;
	private int first, second, third, fourth, fifth;

	public Data() {
	}

	public Data(int first, int second, int third, int fourth, int fifth) {
		this.first = first;
		this.second = second;
		this.third = third;
		this.fourth = fourth;
		this.fifth = fifth;
		firstWord = Integer.toString(first);
		secondWord = Integer.toString(second);
		thirdWord = Integer.toString(third);
		fourthWord = Integer.toString(fourth);
		fifthWord = Integer.toString(fifth);
	}

	public String getFirstWord() {
		return firstWord;
	}

	public void setFirstWord(String firstWord) {
		this.firstWord = firstWord;
	}

	public String getSecondWord() {
		return secondWord;
	}

	public void setSecondWord(String secondWord) {
		this.secondWord = secondWord;
	}

	public String getThirdWord() {
		return thirdWord;
	}

	public void setThirdWord(String thirdWord) {
		this.thirdWord = thirdWord;
	}

	public String getFourthWord() {
		return fourthWord;
	}

	public void setFourthWord(String fourthWord) {
		this.fourthWord = fourthWord;
	}

	public String getFifthWord() {
		return fifthWord;
	}

	public void setFifthWord(String fifthWord) {
		this.fifthWord = fifthWord;
	}

	public int getFirst() {
		return first;
	}

	public void setFirst(int first) {
		this.first = first;
	}

	public int getSecond() {
		return second;
	}

	public void setSecond(int second) {
		this.second = second;
	}

	public int getThird() {
		return third;
	}

	public void setThird(int third) {
		this.third = third;
	}

	public int getFourth() {
		return fourth;
	}

	public void setFourth(int fourth) {
		this.fourth = fourth;
	}

	public int getFifth() {
		return fifth;
	}

	public void setFifth(int fifth) {
		this.fifth = fifth;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + fifth;
		result = prime * result + ((fifthWord == null) ? 0 : fifthWord.hashCode());
		result = prime * result + first;
		result = prime * result + ((firstWord == null) ? 0 : firstWord.hashCode());
		result = prime * result + fourth;
		result = prime * result + ((fourthWord == null) ? 0 : fourthWord.hashCode());
		result = prime * result + second;
		result = prime * result + ((secondWord == null) ? 0 : secondWord.hashCode());
		result = prime * result + third;
		result = prime * result + ((thirdWord == null) ? 0 : thirdWord.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Data other = (Data) obj;
		if (fifth != other.fifth)
			return false;
		if (fifthWord == null) {
			if (other.fifthWord != null)
				return false;
		} else if (!fifthWord.equals(other.fifthWord))
			return false;
		if (first != other.first)
			return false;
		if (firstWord == null) {
			if (other.firstWord != null)
				return false;
		} else if (!firstWord.equals(other.firstWord))
			return false;
		if (fourth != other.fourth)
			return false;
		if (fourthWord == null) {
			if (other.fourthWord != null)
				return false;
		} else if (!fourthWord.equals(other.fourthWord))
			return false;
		if (second != other.second)
			return false;
		if (secondWord == null) {
			if (other.secondWord != null)
				return false;
		} else if (!secondWord.equals(other.secondWord))
			return false;
		if (third != other.third)
			return false;
		if (thirdWord == null) {
			if (other.thirdWord != null)
				return false;
		} else if (!thirdWord.equals(other.thirdWord))
			return false;
		return true;
	}

}
