package org.abdr.tp.projet;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;

import javax.servlet.http.HttpServletResponse;

import org.abdr.http.HttpClient;
import org.abdr.http.response.Response;
import org.abdr.node.Node;
import org.abdr.transaction.SerializableKey;
import org.abdr.transaction.WriteTransaction;
import org.abdr.transaction.impl.StandardWriteTransaction;

public class Benchmark implements Callable<List<Long>> {

	static public final int nbIter = 100;

	protected final Node node;
	protected int val;

	public Benchmark(Node node, int val) {
		this.node = node;
		this.val = val;
	}

	@Override
	public List<Long> call() throws Exception {
		List<Long> time = new ArrayList<>();
		try (HttpClient client = new HttpClient()) {
			//Response r = client.writeTransaction(createStandard(), node);
			for (int i = 0; i < 100; i++) {
				Response r = client.writeTransaction(createOne(), node);
				assert (r.getStatus() == HttpServletResponse.SC_OK) : "Le code d'erreur est " + r.getStatus();
				time.add(r.getTime());
			}
		}
		return time;
	}

	protected String createKey() {
		return "P" + val;
	}
	
	protected final WriteTransaction createOne() throws IOException {
		String profile = createKey();
		Map<SerializableKey, byte[]> values = new HashMap<>(100);
		Data d = new Data();
		SerializableKey key = new SerializableKey(profile, profile);
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		ObjectOutputStream oos = new ObjectOutputStream(out);
		oos.writeObject(d);
		oos.flush();
		values.put(key, out.toByteArray());
		out.close();
		oos.close();

		return new StandardWriteTransaction(new ArrayList<>(values.keySet()), values);
	}

	protected final WriteTransaction createStandardMultiObj(Set<String> cache) throws IOException {
		Map<SerializableKey, byte[]> values = new HashMap<>(100);
		List<String> st = new ArrayList<>(cache);
		int size = cache.size();
		for (int i = 0; i < 100; i++) {
			Data d = new Data(i, i, i, i, i);
			SerializableKey key = new SerializableKey(st.get(i % size));
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			ObjectOutputStream oos = new ObjectOutputStream(out);
			oos.writeObject(d);
			oos.flush();
			values.put(key, out.toByteArray());
			out.close();
			oos.close();
		}
		return new StandardWriteTransaction(new ArrayList<>(values.keySet()), values);
	}

	protected final WriteTransaction createStandard() throws IOException {
		String profile = createKey();
		Map<SerializableKey, byte[]> values = new HashMap<>(100);
		for (int i = 0; i < 100; i++) {
			Data d = new Data(i, i, i, i, i);
			SerializableKey key = new SerializableKey(profile, Integer.toString(i));
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			ObjectOutputStream oos = new ObjectOutputStream(out);
			oos.writeObject(d);
			oos.flush();
			values.put(key, out.toByteArray());
			out.close();
			oos.close();
		}
		return new StandardWriteTransaction(new ArrayList<>(values.keySet()), values);
	}

}
