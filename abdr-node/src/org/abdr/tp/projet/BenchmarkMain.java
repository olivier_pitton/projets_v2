package org.abdr.tp.projet;

import java.io.FileOutputStream;
import java.util.List;

import org.abdr.configuration.Configuration;
import org.abdr.node.Node;
import org.abdr.node.NodeFactory;
import org.eclipse.jetty.util.log.Log;

public class BenchmarkMain {

	public static void main(String[] args) throws InterruptedException {
		Configuration conf = Configuration.getConfiguration(args, "monitor.properties");
		final Node monitor = new NodeFactory().createRemoteNode(conf.getHost(), conf.getPort());
		double app = Math.random() * 100000.0;
		int nbThreads = getNbThreads(args);
		Thread[] threads = new Thread[nbThreads];
		for (int i = 0; i < nbThreads; i++) {
			threads[i] = new Thread(new ExecRunnable(monitor, i, app));
		}
		for (Thread t : threads) {
			t.start();
		}
		for (Thread t : threads) {
			t.join();
		}
	}

	static private int getNbThreads(String[] args) {
		int nbThreads = 1;
		if (args != null && args.length > 1) {
			try {
				nbThreads = Integer.parseInt(args[1]);
			} catch (NumberFormatException e) {
			}
		}
		return nbThreads;
	}

	static private class ExecRunnable implements Runnable {

		private int pos;
		private Node node;
		private int app;

		public ExecRunnable(Node node, int i, double app) {
			pos = i;
			this.node = node;
			this.app = (int) app;
		}

		@Override
		public void run() {
			try {
				List<Long> res = new MigrationBench(node).call();
				long totalTime = 0L;
				try (FileOutputStream fos = new FileOutputStream("log_" + app + "_" + pos + ".log")) {
					for (int i = 0 ; i < res.size() ; i++) {
						Long v = res.get(i);
						totalTime += v.longValue();
						fos.write(v.toString().getBytes());
						fos.write("\n".getBytes());
						fos.flush();
					}
				}
				Log.info("La moyenne de temps d'une transaction est de " + (totalTime / res.size()) + "ms");
			} catch (Exception e) {
				e.printStackTrace();
			}

		}

	}
}
