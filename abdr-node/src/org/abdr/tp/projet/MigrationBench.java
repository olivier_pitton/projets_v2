package org.abdr.tp.projet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletResponse;

import org.abdr.http.HttpClient;
import org.abdr.http.response.Response;
import org.abdr.node.Node;
import org.abdr.transaction.WriteTransaction;
import org.eclipse.jetty.util.log.Log;

public class MigrationBench extends Benchmark {

	public MigrationBench(Node node) {
		super(node, 0);
	}

	@Override
	public List<Long> call() throws Exception {
		List<Long> time = new ArrayList<>();
		try (HttpClient client = new HttpClient()) {
			Set<String> cache = new HashSet<>();
			for (int i = 0; i < 1000; i++) {
				cache.add(createKey());
				time.add(exec(client, createOne()));
			}
			for (int i = 0; i < 100; i++) {
				time.add(exec(client, createStandard()));
			}
			for (int i = 0; i < 100; i++) {
				time.add(exec(client, createStandardMultiObj(cache)));
			}
		}
		return time;
	}
	
	private long exec(HttpClient client, WriteTransaction tr) throws ClassNotFoundException, IOException {
		this.val = (int) (Math.random() * 10000);
		Response r = client.writeTransaction(tr, node);
		assert (r.getStatus() == HttpServletResponse.SC_OK) : "Le code d'erreur est " + r.getStatus();
		Log.info("La transaction a été effectuée en " + r.getTime() + "ms");
		return r.getTime();
	}

}