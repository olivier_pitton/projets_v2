package org.abdr.node;

import org.abdr.configuration.Configuration;

public class NodeFactory {

  public Node createLocalNode(Configuration conf) {
    return new LocalNode(conf);
  }
  
  public Node createRemoteNode(String host, int port) {
    return new RemoteNode(host, port);
  }
  
}
