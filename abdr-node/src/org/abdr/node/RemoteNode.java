package org.abdr.node;

import java.io.IOException;

import org.abdr.configuration.Configuration;

public class RemoteNode implements Node {

	private String host;
	private int port;
	private boolean dirty = false;

	RemoteNode(String host, int port) {
		this.host = host;
		this.port = port;
	}

	@Override
	public Configuration getConfiguration() {
		return null;
	}

	@Override
	public boolean isMonitor() {
		return false;
	}

	@Override
	public boolean isDirty() {
		return this.dirty;
	}

	@Override
	public void setDirty(boolean dirty) {
		this.dirty = dirty;
	}

	@Override
	public String getHost() {
		return host;
	}

	@Override
	public int getPort() {
		return port;
	}

	@Override
	public Node start() throws Exception {
		return this;
	}

	@Override
	public void close() throws IOException {
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((host == null) ? 0 : host.hashCode());
		result = prime * result + port;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RemoteNode other = (RemoteNode) obj;
		if (host == null) {
			if (other.host != null)
				return false;
		} else if (!host.equals(other.host))
			return false;
		if (port != other.port)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return host + ":" + port;
	}

	@Override
	public int compareTo(Node o) {
		int res = host.compareTo(o.getHost());
		return (res == 0) ? port - o.getPort() : res;
	}
}
