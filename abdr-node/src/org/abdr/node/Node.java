package org.abdr.node;

import java.io.Closeable;

import org.abdr.configuration.Configuration;

public interface Node extends Closeable, Comparable<Node> {

  String getHost();

  int getPort();

  boolean isMonitor();
  
  Configuration getConfiguration();

  Node start() throws Exception;
  
  boolean isDirty();
  
  void setDirty(boolean dirty);
  
  boolean equals(Object obj);

  int hashCode();
}
