package org.abdr.node;

import java.io.IOException;

import org.abdr.configuration.Configuration;
import org.abdr.http.HttpServer;
import org.eclipse.jetty.util.log.Log;

public class LocalNode extends RemoteNode {

	private Configuration configuration;
	private HttpServer server;

	LocalNode(Configuration configuration) {
		super("localhost", configuration.getPort());

		this.configuration = configuration;
	}

	@Override
	public boolean isMonitor() {
		return configuration.getType().equals("monitor");
	}

	@Override
	public Configuration getConfiguration() {
		return configuration;
	}

	@Override
	public synchronized Node start() throws Exception {
		server = new HttpServer(this);
		try {
			Log.info("Le serveur de type " + configuration.getType()
					+ " a démarré sur le port " + configuration.getPort());
			server.start();
		} catch (InterruptedException e) {
		}
		return this;
	}

	@Override
	public synchronized void close() throws IOException {
		server.close();
		Log.info("Le serveur de type " + configuration.getType()
				+ " a été arrêté");
	}

}
