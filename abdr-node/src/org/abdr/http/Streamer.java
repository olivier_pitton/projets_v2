package org.abdr.http;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.abdr.http.monitor.Information;
import org.abdr.http.response.ReadResponse;
import org.abdr.http.response.Response;
import org.abdr.transaction.Transaction;
import org.apache.http.client.methods.CloseableHttpResponse;


public class Streamer {

  static public void stream(HttpServletResponse response, Object ser) throws IOException {
    assert (ser instanceof Serializable);
    try (ObjectOutputStream oos = new ObjectOutputStream(response.getOutputStream())) {
      oos.writeObject(ser);
      oos.flush();
    }
  }

  static public Transaction readTransaction(HttpServletRequest req) throws ClassNotFoundException, IOException {
    try (ObjectInputStream ois = new ObjectInputStream(req.getInputStream())) {
      return (Transaction) ois.readObject();
    }
  }

  static public Information readInformation(CloseableHttpResponse execute) throws IOException, ClassNotFoundException {
    return (Information) readApache(execute);
  }

  static public ReadResponse readData(CloseableHttpResponse execute) throws IOException, ClassNotFoundException {
    return (ReadResponse) readApache(execute);
  }
  
  static public Response readWriteResponse(CloseableHttpResponse execute) throws IOException, ClassNotFoundException {
    return (Response) readApache(execute);
  }


  static public Exception readException(CloseableHttpResponse execute) throws IOException, ClassNotFoundException {
    return (Exception) readApache(execute);
  }

  static private Object readApache(CloseableHttpResponse execute) throws IllegalStateException, IOException, ClassNotFoundException {
    try (ObjectInputStream ois = new ObjectInputStream(execute.getEntity().getContent())) {
      return ois.readObject();
    }
  }
}
