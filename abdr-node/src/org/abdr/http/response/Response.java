package org.abdr.http.response;

public class Response implements java.io.Serializable {

	private long time;
	private int status;

	public Response() {
	}

	public Response(long time, int status) {
		this.time = time;
		this.setStatus(status);
	}

	public long getTime() {
		return time;
	}
	
	public void setTime(long time) {
		this.time = time;
	}

	@Override
	public String toString() {
		return Long.toString(time) + "ms";
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

}