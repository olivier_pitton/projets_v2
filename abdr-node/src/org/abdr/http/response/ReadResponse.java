package org.abdr.http.response;

import java.util.Map;

import org.abdr.transaction.SerializableKey;

public class ReadResponse extends Response {

	private Map<SerializableKey, byte[]> datas;

	public ReadResponse(){}

	public ReadResponse(long time, int status, Map<SerializableKey, byte[]> datas) {
		super(time, status);
		this.datas = datas;
	}

	public Map<SerializableKey, byte[]> getSearch() {
		return datas;
	}

}