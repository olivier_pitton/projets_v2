package org.abdr.http.monitor.algo;

import java.io.IOException;
import java.util.Collection;
import java.util.List;

import org.abdr.http.monitor.NodeExecutor;
import org.abdr.http.monitor.Statistic;
import org.abdr.http.monitor.StatisticManager;
import org.abdr.node.Node;
import org.abdr.transaction.SerializableKey;
import org.abdr.transaction.Transaction;

public class HashNodeAlgorithm implements NodeAlgorithm {

	@Override
	public NodeExecutor getNode(StatisticManager manager, Transaction tr) throws IOException {
		List<SerializableKey> keys = tr.getKeys();
		if (keys.size() == 1) {
			return oneKey(manager, tr, keys.get(0));
		}
		return new BestNodeAlgorithm().getNode(manager, tr);
	}

	private NodeExecutor oneKey(StatisticManager manager, Transaction tr, SerializableKey key) {
		Statistic<Long> statistic = manager.getRepartition().get(key);
		if (tr.isRead() && statistic == null) {
			// Lecture sur une clé qui n'existe pas
			return null;
		}
		// Ecriture "append"
		if (statistic == null) {
			List<Node> nodes = manager.getNodes();
			int selectedNode = (int) (Math.abs(key.hashCode()) * Math.random() % nodes.size());
			return new NodeExecutor(nodes.get(selectedNode), null, tr.getKeys());
		}
		// Update ou read, on doit aller sur le bon noeud
		return new NodeExecutor(statistic.getKey(), null, tr.getKeys());
	}

	@Override
	public List<Node> computeBestNodes(StatisticManager manager, Collection<Node> allNodes) {
		return null;
	}
}
