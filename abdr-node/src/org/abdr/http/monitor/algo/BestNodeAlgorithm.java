package org.abdr.http.monitor.algo;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.abdr.http.monitor.NodeExecutor;
import org.abdr.http.monitor.Statistic;
import org.abdr.http.monitor.StatisticManager;
import org.abdr.node.Node;
import org.abdr.transaction.SerializableKey;
import org.abdr.transaction.Transaction;

public class BestNodeAlgorithm implements NodeAlgorithm {

	@Override
	public NodeExecutor getNode(StatisticManager manager, Transaction tr) throws IOException {
		List<SerializableKey> keys = tr.getKeys();
		Set<Node> cacheNodes = new HashSet<>();
		for (SerializableKey k : keys) {
			Statistic<Long> statistic = manager.getRepartition().get(k);
			// Si la clé n'est pas sur le noeud, on passe SI ce n'est pas une écriture
			if(statistic == null && ! tr.isRead()) {
				continue;
			}
			Node key = statistic.getKey();
			if(key.isDirty()) {
				continue;
			}
			cacheNodes.add(key);
		}
		if (cacheNodes.size() <= 1) {
			// Une lecture sur une clé qui n'existe pas
			if (tr.isRead() && cacheNodes.isEmpty()) {
				return null;
			}
			// Une écriture "append". On prend le meilleur
			// de tous les noeuds
			if (!tr.isRead() && cacheNodes.isEmpty()) {
				List<Node> computeBestNodes = computeBestNodes(manager, manager.getNodes());
				//Log.info("Ecriture append (on prend le premier meilleur noeud) : " + computeBestNodes); 
				if(computeBestNodes.isEmpty()) {
					return null;
				}
				return new NodeExecutor(computeBestNodes.get(0), null, keys);
			} else {
				// Une écriture "update" (il n'y a qu'une clé dans le cache)
				// Une lecture sur un noeud.
				Node next = cacheNodes.iterator().next();
				//Log.info("Ecriture update. La clé est sur le noeud : " + next);
				return (cacheNodes.isEmpty()) ? null : new NodeExecutor(next, null, keys);  
			}
		}
		List<Node> bestNodes = computeBestNodes(manager, cacheNodes);
		Node node = bestNodes.remove(0);
		return new NodeExecutor(node, bestNodes, keys);
	}

	@Override
	public List<Node> computeBestNodes(StatisticManager manager, Collection<Node> allNodes) {
		int size = allNodes.size();
		if (size == 1) {
			return Collections.singletonList(allNodes.iterator().next());
		}
		List<Statistic<Double>> res = new ArrayList<>(size);
		for (Node n : allNodes) {
			// Si le noeud est bloqué, on le zappe
			if(n.isDirty()) {
				continue;
			}
			double quotaCompute = compute(manager, n, manager.getQuota(), manager.getQuotaFactor());
			double distanceCompute = compute(manager, n, manager.getDistance(), manager.getDistanceFactor());
			double cpusCompute = compute(manager, n, manager.getCpus(), manager.getCpusFactor());
			double memoryCompute = compute(manager, n, manager.getMemory(), manager.getMemoryFactor());
			double accessCompute = compute(manager, n, manager.getAccess(), manager.getAccessFactor());
			
			double resultat = quotaCompute - distanceCompute + cpusCompute + memoryCompute - accessCompute;
			res.add(new Statistic<>(n, resultat));
		}
		Collections.sort(res, new Comparator<Statistic<Double>>() {

			@Override
			public int compare(Statistic<Double> o1, Statistic<Double> o2) {
				return (int) (o1.getValue() - o2.getValue());
			}
		});
		List<Node> resNodes = new ArrayList<Node>(res.size());
		if(res.isEmpty()) {
			return resNodes;
		}
		for (Statistic<Double> stat : res) {
			resNodes.add(stat.getKey());
		}
		return resNodes;
	}
	
	private <T extends Number> double compute(StatisticManager manager, Node n, TreeSet<Statistic<T>> datas, double factor) {
		if(datas.isEmpty()) {
			return 0.0;
		}
		Statistic<T> value = manager.get(n, datas);
		if(value == null) {
		  return 0.0;
		}
		double max = datas.first().getValue().doubleValue();
		double current  = value.getValue().doubleValue();
		return current * 100.0 / max * factor;
	}
}
