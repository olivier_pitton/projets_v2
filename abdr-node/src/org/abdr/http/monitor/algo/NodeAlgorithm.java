package org.abdr.http.monitor.algo;

import java.io.IOException;
import java.util.Collection;
import java.util.List;

import org.abdr.http.monitor.NodeExecutor;
import org.abdr.http.monitor.StatisticManager;
import org.abdr.node.Node;
import org.abdr.transaction.Transaction;

public interface NodeAlgorithm {

	NodeExecutor getNode(StatisticManager manager, Transaction tr) throws IOException;
	
	List<Node> computeBestNodes(StatisticManager manager, Collection<Node> allNodes);
	
}
