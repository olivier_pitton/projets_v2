package org.abdr.http.monitor;

import java.io.Closeable;
import java.io.IOException;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.abdr.configuration.Configuration;
import org.abdr.http.monitor.algo.HashNodeAlgorithm;
import org.abdr.http.monitor.algo.NodeAlgorithm;
import org.abdr.node.Node;
import org.abdr.transaction.SerializableKey;
import org.abdr.transaction.Transaction;

public class Router implements Closeable {

	private Timer timer;
	private final StatisticManager manager;
	private final NodeWatcher watcher = new NodeWatcher();
	private final NodeAlgorithm algorithm;

	public Router(Configuration configuration) {
		this.manager = new StatisticManager(configuration);
		this.algorithm = new HashNodeAlgorithm();
		this.timer = new Timer();
		this.timer.scheduleAtFixedRate(new TimerTask() {

			@Override
			public void run() {
				manager.refresh();
			}
		}, 0L, 5000L);
	}

	public void updateKeys(List<SerializableKey> keys, Node node) {
		manager.accessKey(keys, node);
	}

	public NodeExecutor getNodes(Transaction tr) throws IOException {
		synchronized (watcher) {
			List<SerializableKey> keys = tr.getKeys();
			if (keys == null || keys.isEmpty()) {
				return null;
			}
			NodeExecutor executor = algorithm.getNode(manager, tr);
			if(executor == null) {
				return null;
			}
			watcher.access(keys, executor.needMigration());
			if (executor.needMigration()) {
				executor.run();
				manager.updateMigrationKeys(keys, executor.getTo());
				watcher.free(keys);
			}
			return executor;
		}
	}

	@Override
	public void close() throws IOException {
		synchronized (watcher) {
			timer.cancel();
		}
	}
	
	@Override
	public String toString() {
		return manager.toString();
	}
}
