package org.abdr.http.monitor;

import java.io.Serializable;

public class Information implements Serializable {

  private long freeSpace;
  private int cpus;
  private long freeMemory;
  
  public Information() {
  }

  public long getFreeSpace() {
    return freeSpace;
  }

  public void setFreeSpace(long freeSpace) {
    this.freeSpace = freeSpace;
  }

public int getCpus() {
	return cpus;
}

public void setCpus(int cpus) {
	this.cpus = cpus;
}

public long getFreeMemory() {
	return freeMemory;
}

public void setFreeMemory(long freeMemory) {
	this.freeMemory = freeMemory;
}
}
