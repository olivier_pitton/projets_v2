package org.abdr.http.monitor;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.abdr.configuration.Configuration;
import org.abdr.http.HttpClient;
import org.abdr.node.Node;
import org.abdr.transaction.SerializableKey;
import org.eclipse.jetty.util.log.Log;

/**
 * La classe fondamentale pour le ca	lcul de placement. Ce gestionnaire contient toutes les informations sur les noeuds
 * et sur l'état global de l'application (noeuds down ...). Les algorithmes de placement l'utilisent pour prendre des décisions
 * en fonction des données qu'il a récolté. Celui-ci est mis à jour via la méthode {@link #refresh()}.
 *
 */
public class StatisticManager {

	// La liste de tous esclaves
	private final List<Node> nodes;
	// La distance (ping) entre le moniteur et les esclaves
	private final TreeSet<Statistic<Long>> distance = new TreeSet<>();
	// L'espace disque que possède les esclaves, sur leurs partitions
	private final TreeSet<Statistic<Long>> quota = new TreeSet<>();
	// La mémoire libre de chaque noeud
	private final TreeSet<Statistic<Long>> memory = new TreeSet<>();
	// Le nombre de CPUs de chaque noeud
	private final TreeSet<Statistic<Long>> cpus = new TreeSet<>();
	// La répartition des clés et le nombre d'accès à chaque clé
	private final Map<SerializableKey, Statistic<Long>> repartition = new HashMap<>(1000);
	private final TreeSet<Statistic<Long>> access = new TreeSet<>();
	private final Configuration configuration;
	// Le nombre de ping effectué
	private long ping;

	public StatisticManager(Configuration config) {
		this.nodes = config.getSlaves();
		Log.info("Les noeuds esclaves suivants ont été ajoutés : " + nodes);
		this.configuration = config;
		this.ping = 0L;
	}
	
	public List<Node> getNodes() {
		return nodes;
	}

	public TreeSet<Statistic<Long>> getCpus() {
		return cpus;
	}

	public TreeSet<Statistic<Long>> getMemory() {
		return memory;
	}

	public TreeSet<Statistic<Long>> getDistance() {
		return distance;
	}

	public TreeSet<Statistic<Long>> getQuota() {
		return quota;
	}

	public TreeSet<Statistic<Long>> getAccess() {
		return access;
	}

	public Map<SerializableKey, Statistic<Long>> getRepartition() {
		return repartition;
	}

	public double getQuotaFactor() {
		return configuration.getQuotaFactor();
	}

	public double getDistanceFactor() {
		return configuration.getDistanceFactor();
	}
	
	public double getCpusFactor() {
		return configuration.getCpusFactor();
	}
	
	public double getMemoryFactor() {
		return configuration.getMemoryFactor();
	}
	
	public double getAccessFactor() {
		return configuration.getAccessFactor();
	}

	public void updateMigrationKeys(List<SerializableKey> key, Node node) {
		for (SerializableKey k : key) {
			Statistic<Long> statistic = repartition.get(k);
			statistic.node = node;
		}
	}

	public synchronized void accessKey(List<SerializableKey> keys, Node n) {
		Statistic<Long> val = get(n, access);
		if(val == null) {
			access.add(new Statistic<Long>(n, 1L));
		} else {
			access.remove(val);
			access.add(new Statistic<Long>(n, val.getValue() + 1L));
		}
		for (SerializableKey key : keys) {
			Statistic<Long> statistic = repartition.get(key);
			if (statistic == null) {
				statistic = new Statistic<Long>(n, 1L);
				repartition.put(key, statistic);
			} else {
				long newValue = statistic.getValue() + 1L;
				statistic.setValue(newValue);
			}
		}
	}

	public void refresh() {
		refreshAll();
		// TODO : Provoquer des migrations
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		/*sb.append("Locality");
		sb.append("\n  Factor : " + getDistanceFactor());
		sb.append("\n  Values : " + distance);
		sb.append("\nDisk Quota");
		sb.append("\n  Factor : " + getQuotaFactor());
		sb.append("\n  Values : " + quota);
		sb.append("\nMemory");
		sb.append("\n  Factor : " + getMemoryFactor());
		sb.append("\n  Values : " + memory);
		sb.append("\nCPUs");
		sb.append("\n  Factor : " + getCpusFactor());
		sb.append("\n  Values : " + cpus);
		sb.append("\nRepartition");*/
		sb.append("\n  Factor : " + getAccessFactor());
		sb.append("\n  Values : " + repartition);
		return sb.toString();
	}

	public <T extends Number> Statistic<T> get(Node node, Set<Statistic<T>> set) {
		for (Statistic<T> stat : set) {
			if (stat.node.equals(node)) {
				return stat;
			}
		}
		return null;
	}

	private void mean(Node node, TreeSet<Statistic<Long>> set, long value) {
		Statistic<Long> stat = get(node, set);
		if(stat == null) {
			set.add(new Statistic<Long>(node, value));
			return;
		}
		long mean = (stat.getValue().longValue() * ping + value) / (ping + 1);
		set.add(new Statistic<Long>(node, mean));
	}
	
	private void refreshAll() {
		ping++;
		try (HttpClient client = new HttpClient()) {
			for (Node node : nodes) {
				try {
					Information info = client.info(node);
					mean(node, quota, info.getFreeSpace());
					mean(node, cpus, info.getCpus());
					long ping = client.ping(node);
					mean(node, distance, ping);
					mean(node, memory, info.getFreeMemory());
					if (node.isDirty()) {
						node.setDirty(false);
					}
				} catch (IOException exception) {
					node.setDirty(true);
					Log.info("Le noeud " + node + " est inaccessible");
				} catch (ClassNotFoundException e) {
					// Can't happen
					e.printStackTrace();
					node.setDirty(true);
				}
			}
		} catch (IOException e1) {
		}
	}
}
