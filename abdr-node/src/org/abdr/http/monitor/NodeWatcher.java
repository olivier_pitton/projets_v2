package org.abdr.http.monitor;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.abdr.transaction.SerializableKey;

public class NodeWatcher {

	private Set<SerializableKey> blocked = new HashSet<>();
	
	public void access(List<SerializableKey> keys, boolean forMigrate) {
		// Lorsqu'un noeud essaye d'accéder à une clé
		synchronized (blocked) {
			int cpt = 0;
			for(SerializableKey key : keys) {
				if(blocked.contains(key)) {
					cpt++;
				}
			}
			if(cpt > 0) {
				try {
					blocked.wait();
				} catch (InterruptedException e) {}
				access(keys, forMigrate);
				return;
			}
			if (forMigrate)
				blocked.addAll(keys);
		}
	}

	public void free(List<SerializableKey> keys) {
		synchronized (blocked) {
			blocked.removeAll(keys);
			blocked.notifyAll();
		}
	}
}
