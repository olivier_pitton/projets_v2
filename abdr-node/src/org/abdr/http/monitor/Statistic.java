package org.abdr.http.monitor;

import java.util.Map;

import org.abdr.node.Node;


public class Statistic<V extends Number> implements Map.Entry<Node, V>, Comparable<Statistic<V>> {

  Node node;
  V value;

  public Statistic(Node node, V value) {
    this.node = node;
    this.value = value;
  }

  @Override
  public Node getKey() {
    return node;
  }

  @Override
  public V getValue() {
    return value;
  }

  @Override
  public V setValue(V value) {
    V old = this.value;
    this.value = value;
    return old;
  }

  @Override
  public int compareTo(Statistic<V> o) {
    return node.compareTo(o.node);
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((node == null) ? 0 : node.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Statistic<?> other = (Statistic<?>) obj;
    if (node == null) {
      if (other.node != null)
        return false;
    } else if (!node.equals(other.node))
      return false;
    return true;
  }

  @Override
  public String toString() {
    return node + " - " + value;
  }
  
}

