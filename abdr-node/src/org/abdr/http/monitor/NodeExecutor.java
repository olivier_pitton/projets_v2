package org.abdr.http.monitor;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.abdr.http.HttpClient;
import org.abdr.http.response.Response;
import org.abdr.node.Node;
import org.abdr.transaction.SerializableKey;
import org.abdr.transaction.impl.MigrationTransaction;
import org.eclipse.jetty.util.log.Log;

public class NodeExecutor implements Iterable<Node> {

	private Node to;
	private List<Node> from;
	private List<SerializableKey> keys;
	private long finalTime;
	
	public NodeExecutor(Node to, List<Node> from, List<SerializableKey> keys) {
		this.to = to;
		this.from = from;
		this.keys = keys;
		finalTime = 0L;
	}

	
	public boolean needMigration() {
		return from != null && !from.isEmpty();
	}

	public List<Node> getFrom() {
		return from;
	}

	public List<SerializableKey> getKeys() {
		return keys;
	}

	public Node getTo() {
		return to;
	}

	public boolean run() throws IOException {
		try (HttpClient client = new HttpClient()) {
			for (int i = 0; i < from.size(); i++) {
				Node n = from.get(i);
				Log.info("Lancement d'une migration du noeud " + n + " vers le noeud " + to);

				try {
					Response statusCode = client.writeTransaction(new MigrationTransaction(keys, to), n);
					assert (statusCode.getStatus() == HttpServletResponse.SC_OK);
					// TODO : Gérer le cas où la migration échoue
					if (statusCode.getStatus() == HttpServletResponse.SC_OK) {
						Log.info("La migration du noeud " + n + " vers le noeud " + to + " a réussie");
						finalTime += statusCode.getTime();
					} else {
						Log.info("La migration du noeud " + n + " vers le noeud " + to + " a échouée");
						return false;
					}
				} catch (ClassNotFoundException e) {
					return false;
				}
			}
		}
		return true;
	}
	
	public long getFinalTime() {
		return finalTime;
	}

	@Override
	public Iterator<Node> iterator() {
		return from.iterator();
	}

}
