package org.abdr.http.monitor;

import java.io.Closeable;
import java.io.IOException;
import java.io.ObjectInputStream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.abdr.http.HttpClient;
import org.abdr.http.Streamer;
import org.abdr.http.response.ReadResponse;
import org.abdr.http.response.Response;
import org.abdr.transaction.ReadTransaction;
import org.abdr.transaction.Transaction;
import org.abdr.transaction.WriteTransaction;
import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.handler.AbstractHandler;
import org.eclipse.jetty.util.log.Log;

public class MonitorHandler extends AbstractHandler implements Closeable {

	private Router router;
	private HttpClient client;

	public MonitorHandler(Router router) {
		this.router = router;
		client = new HttpClient();
	}

	@Override
	public void handle(String target, Request req, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		req.setHandled(true);
		String method = request.getMethod();
		int res = HttpServletResponse.SC_BAD_REQUEST;
		Transaction tr = getTransaction(request);
		if (tr == null) {
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			return;
		}
		if (method.equalsIgnoreCase("post")) {
			if(tr.isRead()) {
				res = read((ReadTransaction) tr, request, response);
			} else {
				res = write((WriteTransaction) tr, request, response);
			}
			
		} 
		response.setStatus(res);
	}

	public Transaction getTransaction(HttpServletRequest request) throws IOException {
		try (ObjectInputStream ois = new ObjectInputStream(request.getInputStream())) {
			try {
				return (Transaction) ois.readObject();
			} catch (ClassNotFoundException exception) {
			}
		}
		return null;
	}

	private int read(ReadTransaction tr, HttpServletRequest request, HttpServletResponse response) throws IOException {
		NodeExecutor node = router.getNodes(tr);
		// Impossible de faire la migration ou aucun noeud n'a été trouvé
		if (node == null || node.getTo() == null) {
			return HttpServletResponse.SC_NO_CONTENT;
		}
		try {
			ReadResponse res = client.readTransaction(tr, node.getTo());
			router.updateKeys(tr.getKeys(), node.getTo());
			res.setTime(node.getFinalTime() + res.getTime());
			Streamer.stream(response, res);
		} catch (ClassNotFoundException exception) {
			return HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
		}
		return HttpServletResponse.SC_OK;
	}

	private int write(WriteTransaction tr, HttpServletRequest request, HttpServletResponse response) throws IOException {
		NodeExecutor route = router.getNodes(tr);
		// Impossible de faire la migration
		if (route == null || route.getTo() == null) {
			Log.info("Aucune route n'a été trouvée pour la transaction");
			return HttpServletResponse.SC_NO_CONTENT;
		}
		try {
			Response res = client.writeTransaction((WriteTransaction) tr, route.getTo());
			router.updateKeys(tr.getKeys(), route.getTo());
			res.setTime(route.getFinalTime() + res.getTime());
			Streamer.stream(response, res);
			return HttpServletResponse.SC_OK;
		} catch (ClassNotFoundException e) {
			return HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
		}
	}

	@Override
	public void close() throws IOException {
		client.close();
		router.close();
	}
}
