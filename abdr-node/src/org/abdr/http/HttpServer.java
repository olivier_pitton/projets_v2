package org.abdr.http;

import java.io.Closeable;
import java.io.IOException;
import java.util.List;

import org.abdr.configuration.Configuration;
import org.abdr.http.monitor.MonitorHandler;
import org.abdr.http.monitor.Router;
import org.abdr.http.slave.SlaveHandler;
import org.abdr.node.Node;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.util.log.Log;

public class HttpServer implements Closeable {

	private final Node node;
	private Server server;

	public HttpServer(Node conf) {
		node = conf;
	}

	public void start() throws Exception {
		Configuration configuration = node.getConfiguration();
		server = new Server(configuration.getPort());
		server.setStopAtShutdown(true);
		if (node.isMonitor()) {
			// Si aucun esclave n'est configué, on kill l'application (aucun
			// intérêt)
			List<Node> nodes = configuration.getSlaves();
			if (nodes.isEmpty()) {
				Log.warn("Il n'y a aucun noeud esclave. L'application ne peut démarrer.");
				System.exit(-2);
			}
			server.setHandler(new MonitorHandler(new Router(configuration)));
		} else {
			server.setHandler(new SlaveHandler(configuration));
		}
		server.start();
		server.join();
	}

	@Override
	public void close() throws IOException {
		if (node.isMonitor()) {
			MonitorHandler handler = (MonitorHandler) server.getHandler();
			handler.close();
		}
		server.destroy();
	}
}
