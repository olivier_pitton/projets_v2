package org.abdr.http.slave;

import java.io.Closeable;
import java.io.IOException;

import oracle.kv.FaultException;
import oracle.kv.KVStore;
import oracle.kv.KVStoreConfig;
import oracle.kv.KVStoreFactory;

import org.abdr.configuration.Configuration;
import org.abdr.transaction.Transaction;
import org.eclipse.jetty.util.log.Log;

public class StoreExecutor implements Closeable {

  private KVStore store;
  private Configuration configuration;

  public StoreExecutor(Configuration conf) throws IOException {
    configuration = conf;
    String host = configuration.getStoreHost() + ":" + configuration.getStorePort();
    KVStoreConfig storeConfig = new KVStoreConfig(configuration.getStoreName(), host);
    try {
      store = KVStoreFactory.getStore(storeConfig);
    } catch (FaultException e) {
      Log.warn("Impossible de se connecter au serveur KVStore " + host);
      throw new IOException(e);
    }
    Log.info("Connexion au serveur KVstore " + host + " effectuée.");
  }

  public Exception execute(Transaction tr) {
    try {
      tr.begin(store);
      tr.commit(store);
      return null;
    } catch (Exception exception) {
      try {
        tr.abort(store, exception);
        return exception;
      } catch (Exception exception1) {
        return exception;
      }
    }
  }

  @Override
  public void close() {
    store.close();
  }
}
