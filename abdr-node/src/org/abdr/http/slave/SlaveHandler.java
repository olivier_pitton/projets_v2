package org.abdr.http.slave;

import java.io.File;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.abdr.configuration.Configuration;
import org.abdr.http.Streamer;
import org.abdr.http.monitor.Information;
import org.abdr.http.response.ReadResponse;
import org.abdr.http.response.Response;
import org.abdr.transaction.ReadTransaction;
import org.abdr.transaction.Transaction;
import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.handler.AbstractHandler;
import org.eclipse.jetty.util.log.Log;

public class SlaveHandler extends AbstractHandler {

	private StoreExecutor executor;

	public SlaveHandler(Configuration conf) throws IOException {
		executor = new StoreExecutor(conf);
	}

	@Override
	public void handle(String target, Request req, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		req.setHandled(true);
		// Si c'est un HEAD, le master fait un ping
		if (request.getMethod().equalsIgnoreCase("HEAD")) {
			response.setStatus(HttpServletResponse.SC_OK);
			return;
		}
		// Si c'est un GET, le master veut des informations sur mon état
		if (request.getMethod().equalsIgnoreCase("GET")) {
			processInformation(response);
			return;
		}
		// Sinon, c'est une transaction a exécuté
		try {
			// On récupère la transaction
			Transaction tr = Streamer.readTransaction(request);
			long start = System.currentTimeMillis();
			// On exécute la transaction
			Exception res = executor.execute(tr);
			long computedTime = (System.currentTimeMillis() - start);
			if (res != null) {
				// La transaction a lancé une erreur. On la renvoie au serveur
				Log.info("La transaction " + tr + " a échoué");
				response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
				Streamer.stream(response, res);
				return;
			}
			// Si pas d'exception, alors cela a réussi.
			Log.info("La transaction " + tr + " a réussi en " + computedTime + "ms");
			int status = HttpServletResponse.SC_OK;
			response.setStatus(status);
			if (tr.isRead()) {
				// Si c'était une transaction de lecture, on envoie aussi les
				// résultats de la recherche
				ReadTransaction read = (ReadTransaction) tr;
				ReadResponse readResponse = new ReadResponse(computedTime, status,read.getSearch());
				Streamer.stream(response, readResponse);
			} else {
				// On envoie le temps d'exécution
				Response writeResponse = new Response(computedTime, status);
				Streamer.stream(response, writeResponse);
			}
		} catch (ClassNotFoundException exception) {
			// Can't happen
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			Streamer.stream(response, exception);
			exception.printStackTrace();
		}
	}

	private void processInformation(HttpServletResponse response) throws IOException {
		long usable = File.listRoots()[0].getUsableSpace();
		Information info = new Information();
		info.setFreeSpace(usable);
		info.setCpus(Runtime.getRuntime().availableProcessors());
		com.sun.management.OperatingSystemMXBean bean =
			  (com.sun.management.OperatingSystemMXBean)
			    java.lang.management.ManagementFactory.getOperatingSystemMXBean();
			info.setFreeMemory(bean.getFreePhysicalMemorySize());
		Streamer.stream(response, info);
		response.setStatus(HttpServletResponse.SC_OK);
	}
}
