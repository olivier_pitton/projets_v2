package org.abdr.http;

import java.io.Closeable;
import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.abdr.http.monitor.Information;
import org.abdr.http.response.ReadResponse;
import org.abdr.http.response.Response;
import org.abdr.node.Node;
import org.abdr.transaction.ReadTransaction;
import org.abdr.transaction.WriteTransaction;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpHead;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.SerializableEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.eclipse.jetty.util.log.Log;

public class HttpClient implements Closeable {

  private CloseableHttpClient httpclient = HttpClients.createDefault();

  public ReadResponse readTransaction(ReadTransaction tr, Node node) throws IOException, ClassNotFoundException {
    HttpPost post = new HttpPost(String.format("http://%s:%d/", node.getHost(), node.getPort()));
    post.setEntity(new SerializableEntity(tr, true));
    Log.info("Envoi de la transaction " + tr + " sur le noeud " + node);
    try(CloseableHttpResponse execute = httpclient.execute(post)) {
      int statusCode = execute.getStatusLine().getStatusCode();
      if(statusCode == HttpServletResponse.SC_BAD_REQUEST) {
        throw new IOException(Streamer.readException(execute));
      } else if(statusCode == HttpServletResponse.SC_OK) {
        return Streamer.readData(execute);
      } 
    }
    Log.warn("Erreur interne durant l'invocation de la transaction.");
    return null;
  }
  
  public Response writeTransaction(WriteTransaction tr, Node node) throws IOException, ClassNotFoundException {
    HttpPost post = new HttpPost(String.format("http://%s:%d/", node.getHost(), node.getPort()));
    post.setEntity(new SerializableEntity(tr, true));
    Log.info("Envoi de la transaction " + tr + " sur le noeud " + node);
    try(CloseableHttpResponse execute = httpclient.execute(post)) {
      int status = execute.getStatusLine().getStatusCode();
      if(status == HttpServletResponse.SC_OK) {
      	return Streamer.readWriteResponse(execute);
      }
      Exception e = Streamer.readException(execute);
      throw new IOException(e);
    }
  }
  
  public long ping(Node node) throws IOException {
    HttpHead head = new HttpHead(String.format("http://%s:%d/", node.getHost(), node.getPort()));
    long start = System.currentTimeMillis();
    try(CloseableHttpResponse execute = httpclient.execute(head)) {      
    }
    return (System.currentTimeMillis() - start);
  }
  
  public Information info(Node node) throws IOException, ClassNotFoundException {
    HttpGet head = new HttpGet(String.format("http://%s:%d/", node.getHost(), node.getPort()));
    try(CloseableHttpResponse execute = httpclient.execute(head)) {
      return Streamer.readInformation(execute);
    }
  }

  @Override
  public void close() throws IOException {
    httpclient.close();
  }
  
  

}
