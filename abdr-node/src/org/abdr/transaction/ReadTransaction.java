package org.abdr.transaction;

import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import oracle.kv.KVStore;
import oracle.kv.ValueVersion;

public class ReadTransaction implements Transaction {

	protected List<SerializableKey> keys;
	protected Map<SerializableKey, byte[]> search;

	public ReadTransaction() {
		search = new HashMap<>();
	}

	public ReadTransaction(List<SerializableKey> keys) {
		this();
		this.keys = keys;
	}

	@Override
	public void writeExternal(ObjectOutput out) throws IOException {
		out.writeObject(keys);
		out.writeObject(search);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
		keys = (List<SerializableKey>) in.readObject();
		search = (Map<SerializableKey, byte[]>) in.readObject();
	}

	@Override
	public List<SerializableKey> getKeys() {
		return keys;
	}

	public Map<SerializableKey, byte[]> getSearch() {
		return this.search;
	}

	@Override
	public boolean isRead() {
		return true;
	}

	@Override
	public boolean isMigration() {
		return false;
	}

	@Override
	public void begin(KVStore store) throws Exception {
	}

	@Override
	public void commit(KVStore store) throws Exception {
		for (SerializableKey key : getKeys()) {
			ValueVersion value = store.get(key.getKey());
			if (value != null) {
				search.put(key, value.getValue().getValue());
			}
		}
	}

	@Override
	public void abort(KVStore store, Exception ex) throws Exception {
		search.clear();
	}

	@Override
	public String toString() {
		return "Read " + getKeys().size() + " keys";
	}
}
