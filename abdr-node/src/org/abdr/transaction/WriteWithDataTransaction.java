package org.abdr.transaction;

import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.List;
import java.util.Map;

public abstract class WriteWithDataTransaction extends WriteTransaction {

	protected Map<SerializableKey, byte[]> datas;

	public WriteWithDataTransaction() {
	}

	public WriteWithDataTransaction(List<SerializableKey> keys, Map<SerializableKey, byte[]> values) {
		super(keys);
		datas = values;
	}

	public Map<SerializableKey, byte[]> getDatas() {
		return datas;
	}

	@Override
	public void writeExternal(ObjectOutput out) throws IOException {
		super.writeExternal(out);
		out.writeObject(datas);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
		super.readExternal(in);
		datas = (Map<SerializableKey, byte[]>) in.readObject();
	}
}
