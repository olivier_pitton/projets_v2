package org.abdr.transaction;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import oracle.kv.Key;

public class SerializableKey implements Externalizable,Comparable<SerializableKey> {

  private String major, minor;
  private transient Key key;

  public SerializableKey() {
  }

  public SerializableKey(String major) {
  	this(major, major);
  }
  
  public SerializableKey(String major, String minor) {
    this.major = major;
    this.minor = minor;
    this.key = Key.createKey(major, minor);
  }

  @Override
  public void writeExternal(ObjectOutput out) throws IOException {
    out.writeUTF(major);
    out.writeUTF(minor);
  }

  @Override
  public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
    major = in.readUTF();
    minor = in.readUTF();
    key = Key.createKey(major, minor);
  }

  public String getMajor() {
    return major;
  }

  public void setMajor(String major) {
    this.major = major;
  }

  public String getMinor() {
    return minor;
  }

  public void setMinor(String minor) {
    this.minor = minor;
  }

  public Key getKey() {
    return key;
  }

  public void setKey(Key key) {
    this.key = key;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((major == null) ? 0 : major.hashCode());
    result = prime * result + ((minor == null) ? 0 : minor.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    SerializableKey other = (SerializableKey) obj;
    if (major == null) {
      if (other.major != null)
        return false;
    } else if (!major.equals(other.major))
      return false;
    if (minor == null) {
      if (other.minor != null)
        return false;
    } else if (!minor.equals(other.minor))
      return false;
    return true;
  }

  @Override
  public String toString() {
    return key.toString();
  }

	@Override
	public int compareTo(SerializableKey o) {
		return key.compareTo(o.key);
	}

}
