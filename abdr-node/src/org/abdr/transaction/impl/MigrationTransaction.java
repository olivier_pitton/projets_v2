package org.abdr.transaction.impl;

import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import oracle.kv.KVStore;
import oracle.kv.ValueVersion;

import org.abdr.http.HttpClient;
import org.abdr.http.response.Response;
import org.abdr.node.Node;
import org.abdr.node.NodeFactory;
import org.abdr.transaction.SerializableKey;
import org.abdr.transaction.WriteTransaction;

public class MigrationTransaction extends WriteTransaction {

	protected String host;
	protected int port;

	public MigrationTransaction() {
		super();
	}

	public MigrationTransaction(List<SerializableKey> keys, Node node) {
		this(keys, node.getHost(), node.getPort());
	}

	public MigrationTransaction(List<SerializableKey> keys, String host, int port) {
		super(keys);
		this.host = host;
		this.port = port;
	}

	@Override
	public void writeExternal(ObjectOutput out) throws IOException {
		super.writeExternal(out);
		out.writeUTF(host);
		out.writeInt(port);
	}

	@Override
	public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
		super.readExternal(in);
		host = in.readUTF();
		port = in.readInt();
	}

	@Override
	public void begin(KVStore store) throws Exception {

	}

	@Override
	public void commit(KVStore store) throws Exception {
		Map<SerializableKey, byte[]> datas = new HashMap<>();
		List<SerializableKey> realKeys = new ArrayList<>();
		for (SerializableKey key : this.getKeys()) {
			ValueVersion valueVersion = store.get(key.getKey());
			if (valueVersion != null) {
				datas.put(key, valueVersion.getValue().getValue());
				realKeys.add(key);
			}
		}
		try (HttpClient client = new HttpClient()) {
			Node node = new NodeFactory().createRemoteNode(host, port);
			Response resultat = client.writeTransaction(new MigrationClientTransaction(realKeys, datas), node);
			if (resultat.getStatus() != HttpServletResponse.SC_OK) {
				throw new IOException("La migration vers le noeud " + node + " a échouée");
			}
		}
		for (SerializableKey key : this.getKeys()) {
			store.delete(key.getKey());
		}
	}

	@Override
	public void abort(KVStore store, Exception ex) throws Exception {
		ex.printStackTrace();
	}

	@Override
	public String toString() {
		return "Migration vers le noeud " + host + ":" + port + " de " + this.keys.size() + " keys";
	}

}
