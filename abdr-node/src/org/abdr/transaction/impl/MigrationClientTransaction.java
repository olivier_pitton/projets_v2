package org.abdr.transaction.impl;

import java.util.List;
import java.util.Map;

import oracle.kv.KVStore;
import oracle.kv.Value;

import org.abdr.transaction.SerializableKey;
import org.abdr.transaction.WriteWithDataTransaction;

public class MigrationClientTransaction extends WriteWithDataTransaction {

  public MigrationClientTransaction() {
  }

  public MigrationClientTransaction(List<SerializableKey> keys, Map<SerializableKey, byte[]> values) {
    super(keys, values);
  }

  @Override
  public void begin(KVStore store) throws Exception {

  }

  @Override
  public void commit(KVStore store) throws Exception {
    for (Map.Entry<SerializableKey, byte[]> entry : this.datas.entrySet()) {
      SerializableKey key = entry.getKey();
      store.put(key.getKey(), Value.createValue(entry.getValue()));
    }
  }

  @Override
  public void abort(KVStore store, Exception ex) throws Exception {
    for (Map.Entry<SerializableKey, byte[]> entry : this.datas.entrySet()) {
      store.delete(entry.getKey().getKey());
    }
  }
  
  @Override
  public String toString() {
  	return "Migration cliente de " + this.datas.size() + " keys";
  }
}
