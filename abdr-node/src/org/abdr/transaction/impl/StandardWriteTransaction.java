package org.abdr.transaction.impl;

import java.util.List;
import java.util.Map;

import oracle.kv.KVStore;
import oracle.kv.Value;

import org.abdr.transaction.SerializableKey;
import org.abdr.transaction.WriteWithDataTransaction;

public class StandardWriteTransaction extends WriteWithDataTransaction {

	public StandardWriteTransaction() {
	}

	public StandardWriteTransaction(List<SerializableKey> keys, Map<SerializableKey, byte[]> values) {
		super(keys, values);
	}

	@Override
	public void begin(KVStore store) throws Exception {
	}

	@Override
	public void commit(KVStore store) throws Exception {
		for (Map.Entry<SerializableKey, byte[]> val : datas.entrySet()) {
			store.put(val.getKey().getKey(), Value.createValue(val.getValue()));
		}
	}

	@Override
	public void abort(KVStore store, Exception ex) throws Exception {
		for (Map.Entry<SerializableKey, byte[]> val : datas.entrySet()) {
			store.delete(val.getKey().getKey());
		}
	}

	@Override
	public String toString() {
		return "Write " + keys.size() + " keys";
	}
}
