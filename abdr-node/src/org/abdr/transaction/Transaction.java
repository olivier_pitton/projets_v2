package org.abdr.transaction;

import java.io.Externalizable;
import java.util.List;

import oracle.kv.KVStore;

public interface Transaction extends Externalizable {

  void begin(KVStore store) throws Exception;

  void commit(KVStore store) throws Exception;
  
  void abort(KVStore store, Exception ex) throws Exception;

  List<SerializableKey> getKeys();
  
  boolean isRead();
  
  boolean isMigration();
}
