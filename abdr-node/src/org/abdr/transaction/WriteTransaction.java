package org.abdr.transaction;

import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.List;

public abstract class WriteTransaction implements Transaction {

  protected List<SerializableKey> keys;
  
  public WriteTransaction() {
    super();
  }

  public WriteTransaction(List<SerializableKey> keys) {
    this.keys = keys;
  }

  @Override
  public void writeExternal(ObjectOutput out) throws IOException {
    out.writeObject(keys);
  }

  @SuppressWarnings("unchecked")
  @Override
  public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
    keys = (List<SerializableKey>) in.readObject();
  }

  @Override
  public List<SerializableKey> getKeys() {
    return keys;
  }

  @Override
  public boolean isRead() {
    return false;
  }

  @Override
  public boolean isMigration() {
    return false;
  }
}
