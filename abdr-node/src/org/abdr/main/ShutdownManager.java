package org.abdr.main;

import java.io.IOException;

import org.abdr.node.Node;
import org.eclipse.jetty.util.log.Log;

public class ShutdownManager implements Runnable{

	private final Node node;
	
	
	public ShutdownManager(Node node) {
		this.node = node;
	}
	
	@Override
	public void run()  {
		Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
			
			@Override
			public void run() {
				try {
					node.close();
				} catch (IOException e) {}				
			}
		},"Shutdown Thread"));
		Log.info("Mise en place du hook d'arrêt");
	}

}
