package org.abdr.main;

import org.abdr.configuration.Configuration;
import org.abdr.node.Node;
import org.abdr.node.NodeFactory;

public class MonitorMain {

	public static void main(String[] args) throws Exception {
		Configuration conf = Configuration.getConfiguration(args, "monitor.properties");
		Node node = new NodeFactory().createLocalNode(conf);
		new ShutdownManager(node).run();
		node.start();
	}
}
