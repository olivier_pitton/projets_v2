package org.abdr.configuration;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.abdr.node.Node;
import org.abdr.node.NodeFactory;

public class Configuration {

	static public Configuration getConfiguration(String[] args, String defaultFile) {
		String file = (args == null || args.length == 0) ? defaultFile : args[0];
		try {
			System.out.println("Le fichier de configuration choisi est : " + file);
			return new Configuration(file);
		} catch (IOException exception) {
			System.out.println("Impossible de charger le fichier de configuration");
			System.exit(-1);
		}
		return null;
	}
	
	private Properties properties = new Properties();

	public Configuration(String filename) throws IOException {
		properties.load(new FileReader(filename));
	}

	public String getHost() {
		return properties.getProperty("server.host", "localhost");
	}

	public double getQuotaFactor() {
		return Double.parseDouble(properties.getProperty("monitor.quota.factor", "0.5"));
	}

	public double getAccessFactor() {
		return Double.parseDouble(properties.getProperty("monitor.access.factor", "4.0"));
	}

	public double getMemoryFactor() {
		return Double.parseDouble(properties.getProperty("monitor.memory.factor", "0.2"));
	}

	public double getCpusFactor() {
		return Double.parseDouble(properties.getProperty("monitor.cpus.factor", "0.3"));
	}

	public double getDistanceFactor() {
		return Double.parseDouble(properties.getProperty("monitor.distance.factor", "0.5"));
	}

	public String getStoreName() {
		return properties.getProperty("slave.kvstore.name", "kvstore");
	}

	public String getStoreHost() {
		return properties.getProperty("slave.kvstore.host", "localhost");
	}

	public String getStorePath() {
		return properties.getProperty("slave.kvstore.path", "/tmp/kvroot");
	}

	public int getStorePort() {
		return Integer.parseInt(properties.getProperty("slave.kvstore.port", "5000"));
	}

	public int getPort() {
		return Integer.parseInt(properties.getProperty("server.port", "8080"));
	}

	public String getType() {
		return properties.getProperty("server.type", "slave");
	}

	public List<Node> getSlaves() {
		String property = properties.getProperty("monitor.slaves");
		if (property == null) {
			return new ArrayList<>(0);
		}

		String[] slaves = property.split(",");
		List<Node> list = new ArrayList<>(slaves.length);
		NodeFactory factory = new NodeFactory();
		for (String slave : slaves) {
			String[] val = slave.split(":");
			list.add(factory.createRemoteNode(val[0].trim(), Integer.parseInt(val[1].trim())));
		}
		return list;
	}
}
