\documentclass[a4paper,10pt]{article}
\usepackage[top=1.5cm, bottom=2cm, left=2cm, right=2cm]{geometry}
\usepackage[utf8]{inputenc}
\usepackage[francais]{babel}
\usepackage{alltt}
\usepackage{graphicx}

\title{Projet ABDR}
\author{Domin Wandrille et Pitton Olivier}
\date{5 janvier 2014}

\begin{document}
\maketitle

\newpage
\part{Introduction}

Notre objectif à travers ce projet a été de réaliser une véritable application répartie permettant l'exécution de transactions sur des noeuds KVStore. 
Nous avons chercher à aller plus loin que le sujet, tout en le respectant. Ce rapport s'articule en deux grandes parties. Tout d'abord la description détaillée
de la solution proposée : architecture logicielle, algorithmes de routage de transactions, tolérance aux pannes, ... puis l'ensemble des réponses
aux questions posées dans le sujet. Enfin, nous conclurons sur le travail accompli ces dernières semaines.

\newpage
\part{Le projet}
\section{Présentation de l'architecture}
Les architectures d'applications réparties se regroupent en deux grandes familles : le pair à pair et le maître-esclave. Ces architectures ont chacunes
leurs spécificités, avantages et inconvénients. Nous nous sommes portés sur une solution de type maître-esclave. Notre application possède donc un maître, appelé moniteur, et
un ensemble d'esclaves associés à un noeud KVStore.\newline
\newline
Chaque esclave possède une unique connexion à un noeud KVStore, le moniteur possède une connexion par esclave.

\begin{figure}[h]
   \centering
   \includegraphics[scale=0.35]{images/image1.png}
   \caption{Architecture de l'application répartie comprenant deux esclaves}
\end{figure}

Le mode de fonctionnement est le suivant : Les applications envoient des transactions au moniteur qui se charge de les transmettre intelligemment
à l'esclave concerné. L'intérêt d'une architecture maître-esclave intervient à ce niveau. De part un unique moniteur redirigeant les requêtes vers les esclaves, 
celui-ci est au courant de l'état global de l'application. Quelles sont les transactions en cours ? Combien ? Sur quels noeuds ? C'est en cela que notre
solution répond efficacement au projet, puisque nous sommes en mesure d'effectuer les migrations de la manière la plus efficace qui soit dans la mesure où nous connaissons
l'état de l'ensemble des noeuds de l'application.

\section{Fonctionnement interne}

\subsection{Protocole d'échange}

Nous avons choisi le \textbf{protocole HTTP} pour réaliser l'ensemble des échanges entre les applications et le moniteur, ainsi qu'entre le moniteur et les esclaves.\newline
Les avantages de ce protocole dans ce projet sont : 
\begin{enumerate}
 \item Simplicité d'utilisation : Le serveur est déjà dans les bibliothèques KVStore (Jetty), les codes d'erreurs indiquent les réponses lors de chaque échange, ...
 \item L'hétérogénéité : N'importe quelle application utilisant le protocole HTTP peut intéragir avec notre application, ce qui n'est pas le cas avec Java RMI.
 \item Parallélisme : Le serveur HTTP possède déjà les pools de threads. Nous n'avons pas à réaliser la parallélisation des requêtes, simplement nous assurer d'éventuelles problèmes de concurrence, comme lors d'une migration.
 \item Facilité d'utilisation dans la salle SAR : Par défaut, on ne peut pas utiliser Java RMI sur plusieurs machines sans redéfinir un \textit{Registry}. Cela a été expliqué dans le cours de M1 SRCS.
\end{enumerate}

Pour toutes ces raisons, nous avons décidé d'utiliser HTTP plutôt que d'autres protocoles comme RMI ou CORBA.

\subsection{Transactions}

Notre application peut réaliser la totalité des transactions possibles. Plutôt que de nous focaliser sur un type de transaction, nous laissons la possibilité
aux applications de réaliser leurs propres transactions. Un client va donc écrire sa transaction comme bon lui semble, et l'envoyer au système qui se charge alors de l'exécuter de manière transparente.\newline
Nous pouvons donc assimiler notre solution à un système de bases de données classiques puisque celui-ci met en place les transactions à travers le langage SQL. L'utilisateur est libre d'exécuter n'importe quel requête dans le cadre défini par le système. Notre cadre à nous est les API KVstore.
\newline
\newline
Les transactions peuvent être de type \textit{écriture} ou \textit{lecture}. Le type de la transaction fait varier le routage des requêtes, comme cela est décrit plus bas. Une transaction doit 
donner les clés auxquelles elle va accéder. En effet, nous sommes capables de gérer le multi-clés, l'une des améliorations proposées dans le sujet.
\newline
\newline
Voici l'interface standard d'une transaction que nous utilisons dans l'application.
\begin{verbatim}
public interface Transaction extends Externalizable {

 void begin(KVStore store) throws Exception;

 void commit(KVStore store) throws Exception;

 void abort(KVStore store, Exception ex) throws Exception;

 List<SerializableKey> getKeys();

 boolean isRead();
}
\end{verbatim}
L'exécution d'une transaction sur un noeud esclave se fait par l'appel à la méthode \textit{begin}, puis \textit{commit}. Si une erreur est lancée lors de l'appel à l'une de ces méthodes, la méthode \textit{abort} est appelée. 
La méthode \textit{getKeys} renvoie la liste des clés accédées par la transactions. Enfin, la méthode \textit{isRead} renvoie vrai si la transaction est une lecture, faux si c'est une écriture.
\newline
\newline
Notre objectif derrière cela était de laisser la liberté au client quant aux transactions à réaliser, lui permettant alors d'utiliser la totalité de KVStore sur un système réparti, et cela de manière transparente.
Notre solution a pour vocation de rendre KVStore réparti en offrant un ensemble de fonctionnalités de plus haut niveau, comme le routage. Les transactions sont donc
notre moyen de communiquer entre le client et KVStore, en passant par notre solution.

\subsection{Statistiques}

Afin d'être capable de migrer efficacement les données, nous devons conserver un ensemble de statistiques de l'état de l'application. Cet ensemble nous permettra à la fois de migrer
les données, mais aussi de placer nos données intelligemment. Ces statistiques sont stockées uniquement sur le moniteur ; elles sont mises à jour à la réception
de chaque transaction et toutes les cinq secondes par le biais de requêtes aux esclaves. 
\newline
\newline
Afin d'éviter les faux-positifs, chaque statistique récupérée est lissée, et donc seule la moyenne est conservée. 
Par exemple, si le réseau est saturé durant la mesure pour un noeud, la valeur n'est pas conservée telle quelle mais moyennée avec
toutes les précédentes mesures.
\newline
\newline
Le moniteur possède les statistiques suivantes :
\begin{enumerate}
 \item Latence : La latence entre le moniteur et un esclave ;
 \item Nombre de coeurs : Le nombre de coeurs que possèdent le noeud ;
 \item Mémoire libre : Le nombre d'octet de mémoire RAM disponible sur la machine ;
 \item Espace disque : L'espace disque disponible sur la partition KVStore.
 \item Répartition : La répartition et la fréquence d'utilisation de chaque clé pour chaque noeud. 
\end{enumerate}

Ces informations sont de véritables aides à la décision lors du placement des requêtes. Puisque nous gérons les transactions multi-clés, nous ne pouvons pas simplement utiliser le hachage de la clé. 
Deux possibilités s'offraient à nous : 
\begin{enumerate}
 \item Hacher les clés et faire exécuter plusieurs transactions sur plusieurs noeuds selon la valeur de hachage renvoyée ;
 \item Déterminer à un instant précis quel est le meilleur noeud sur lequel la transaction peut s'exécuter.
\end{enumerate}

Nous avons choisi la deuxième solution car elle apporte une certaine complexité algorithmique. 
Nous devons retrouver les informations essentielles à la décision et écrire un algorithme 
de placement efficace, avec les données que nous avons.

\subsection{Routage des transactions}

Les transactions d'écriture sont routées de la manière suivante :
\begin{enumerate}
 \item Si la transaction ne possède qu'une clé, cette clé est hachée et placée de la manière suivante : $hash \% nbSlaves$ ;
 \item Si la totalité des clés de la transaction se situent sur un noeud, la transaction est envoyée sur ce noeud ;
 \item Si les clés de la transaction sont disséminées sur plusieurs noeuds, une migration est effectuée sur le meilleur noeud parmi ceux possédant au moins une clé. La transaction est routée sur ce noeud à la fin de la transaction.
\end{enumerate}

Les transactions de lecture sont routées de la manière suivante :
\begin{enumerate}
 \item Si aucun noeud n'a été trouvé pour la transaction, c'est que les données ne sont pas présentes, donc un message d'erreur est renvoyé ;
 \item Si la totalité des clés de la transaction se situent sur un noeud, la transaction est envoyée sur ce noeud ;
 \item Si les clés de la transaction sont disséminées sur plusieurs noeuds, une migration est effectuée sur le meilleur noeud parmi ceux possédant au moins une clé. La transaction est routée sur ce noeud à la fin de la transaction.
\end{enumerate}

L'algorithme du meilleur noeud se base sur l'ensemble des statistiques récoltées durant le lancement de la solution. Des facteurs sont appliqués
afin de modifier l'importance de la statistique. Cet algorithme calcule le pourcentage de chaque statistique et y applique le facteur. On
somme le nombre de CPU, de mémoire et d'espace disque, et on soustrait la latence ainsi que le nombre d'accès. Le noeud obtenant le plus grand
nombre est el noeud que l'on choisit. Il est celui ayant le plus de CPU / mémoire libre / espace disque et le moins d'accès / latence.
\begin{verbatim}
 long max;
 Noeud node; 
 long maxCpus = getMaxCpus(); // Le nombre maximum de CPU parmi tous les noeuds
 long maxLatence = getMaxLatence(); // La latence maximale entre le moniteur et un noeud
 long maxMemory = getMaxMemory(); // La mémoire libre maximale parmi tous les noeuds
 long maxDiskStorage = getMaxDiskStorage(); // L'espace disque le plus grand parmi tous les noeuds
 long maxAccess = getMaxAccess(); // Le nombre d'accès maximum parmi tous les noeuds
 
 Pour tous les noeuds esclaves n Faire
  long nbCpus = getCpus(n); // Le nombre de CPU du noeud courant
  long nbLatence = getLatence(n); // La latence du noeud courant
  long nbMemory = getMemory(n); // La mémoire libre du noeud courant
  long nbDiskStorage = getDiskStorage(n); // L'espace libre du noeud courant
  // Le nombre d'accès fait sur le noeud courant. Cela représente la charge de ce noeud.
  long nbAccess = getAccess(n); 
  
  // Les facteurs appliqués aux moyennes sont déterminés par l'administrateur de la 
  // solution et compris entre 0 et 1.
  // Ils permettent de déterminer l'importance de l'information. 
  // Par exemple si l'on veut une application CPU-bound, les facteurs du nombre de CPU 
  // et de la mémoire libre doivent être supérieures à celui de l'espace disque.
  long resultat = (nbCpus / maxCpus * factorCpus) - (nbLatence / maxLatence * factorLatence) + 
    (nbMemory / maxMemory * factorMemory) + (nbDiskStorage / maxDiskStorage * factorDiskStorage)
    - (nbAccess / maxAccess * factorAccess);
  Si resultat > max Faire 
    // On a trouvé le meilleur noeud. Celui ayant, en moyenne, le plus de CPUS,
    // mémoire libre, espace disque et le moins de latence et d'accès.
    max = resultat;
    node = n;
  
  Retourner node
\end{verbatim}

Cet algorithme est appliqué lors de chaque migration.

\subsection{Migration de données}

Lors de la détection de la migration de données, les clés à migrer sont bloquées. Toute transaction désirant accéder de quelque manière que ce soit à
au moins une de ces clés se retrouvent bloqués en attente de la fin de la migration. Le moniteur possède donc un verrou global par lequel passe toutes les 
transactions. Si la transaction ne provoque aucune migration, elle peut être placée comme il se doit. Si elle en provoque une, avant qu'une autre transaction ne prenne
le verrou, les clés sont mises à l'état bloquées, la migration est lancée et la transaction s'effectue. Etant donné que le maître est multi-threadé, une migration peut être en
cours d'exécution pendant qu'une transaction n'accédant pas aux clés de la migration est redirigé vers le noeud adéquat, qui peut être l'un des noeuds en cours de migration.
\newline
\newline
L'intérêt de ce bloquage de baisser le grain de verrouillage, afin d'en obtenir plus fin. On peut l'assimiler au verrouillage par lignes d'un système relationnel,
quant le bloquage d'un noeud résulte d'un verrouillage par table. L'exécution d'un grand nombre de transactions peut donc être réellement fait en parallèle, si elles sont indépendantes.

\subsection{Tolérance aux pannes}

La solution proposée possède à un certain degré une tolérance aux pannes. Ce degré est le suivant :
\begin{enumerate}
 \item Les fautes sont de type transitoire (et donc peuvent être franches) : Un noeud esclave peut ne pas répondre, de manière définitive (crash de la machine) ou transitoire (relancement du programme), il sera détecté par le maître ;
 \item Les canaux sont \textit{quasi-fiables} : Si le maître et l'esclave sont corrects, alors si le maître envoie une transaction l'esclave la recevra.
\end{enumerate}

Le maître envoie toutes les cinq secondes des demandes de statistiques à tous les esclaves. Ces demandes servent aussi de détecteurs de fautes.
\newline
Si un noeud tombe dans l'intervalle des cinq secondes et qu'une transaction lui est envoyée, le maître détecte l'erreur, met l'esclave à l'état \textit{dirty}, et cherche un autre noeud.
Cette opération est répétée jusqu'à ce qu'aucun noeud ne soit disponible.
\newline
Dès lors qu'un noeud retourne dans un état cohérent, le maître le détectera dans les cinq prochaines secondes, au maximum, et le remettra dans un état valide.

\subsection{Exécution du système}
Le projet a été construit à partir de l'outil Ant et d'Eclipse. En exécutant la tâche \textit{ant all}, l'ensemble des JARs seront construits automatiquement. 
Vous trouverez 3 JARs à la fin de l'étape de compilation :
\begin{enumerate}
 \item abdr-node-monitor : Le JAR permettant de lancer le moniteur. Celui-ci doit être lancé après que les esclaves aient démarrés ;
 \item abdr-node-slave : Le JAR permettant de lancer un esclave ; 
 \item abdr-node-test : Le JAR permettant de lancer le benchmark utilisé pour le projet, tel que décrit dans le sujet.
\end{enumerate}

Un lancement standard se fait de la manière suivante :
\begin{enumerate}
 \item Lancement de KVStore ;
 \item Lancement d'un noeud esclave avec la commande : \textit{java -jar abdr-node-slave} ;
 \item Lancement du moniteur avec la commande : \textit{java -jar abdr-node-monitor} ;
 \item Lancement du benchmark avec la commande : \textit{java -jar abdr-node-test monitor.properties <n>} où \textit{n} est le nombre de threads à lancer, soit la valeur de C pour l'application courante.
\end{enumerate}

L'accès aux noeuds, les facteurs de statistiques, ... sont spécifiés dans une configuration. Une configuration par défaut se trouve embarqué pour chaque JAR.

\newpage
\part{Questions}

\section{Partie 1 : Transactions et concurrence}
\subsection{Rédiger un rapport (+ code source commenté) du TmeKvstore.}

Le code source du TME se trouve dans le dossier \textit{src}, dans les packages \textit{org.tp}. 

\paragraph{L'exécution simultanée de 2 programmes M1 conserve-t-elle la cohérence des données ?}
Non. Les insertions effectuées en parallèle peuvent se chevaucher, et donc 'annuler' une précédente insertion. Si on exécute 3 programmes en simultané, le résultat théorique est 3000, mais le résultat pratique est inférieur, et très variable.

\paragraph{Si au départ la quantité vaut 1 pour tous les produits, quelle quantité obtient-on après avoir exécuté 2 fois le programme M2, en série (i.e., l'une après l'autre) ?}
La quantité finale est 2000. La première exécution fait passer l'ensemble des produits de 1 à 1000, puis la seconde de 1000 à 2000.

\paragraph{L'exécution simultanée de 2 programmes M2 conserve-t-elle la cohérence des données (la quantité obtenue est-elle la même qu'avec une exécution en série)?}

Oui. Les transactions qui se terminent en premiers ne possèdent pas le bon résultat car il en reste au moins une en cours d'exécution. En revanche la dernière récupère bien le bon résultat.

\subsection{Rédiger au moins une page pour votre avis personnel sur la faisabilité de traiter des transactions avec un système de stockage de paires {clé,valeur}.}

Une transaction est un groupe d'opérations exécutées qui font passer une base de données d'un état A à un état B et qui possèdent des mécanismes assurant la cohérence et la sureté des opérations. Ces mécanismes sont connus sous le nom de propriétés ACID.
\newline
D'après le théorème de CAP, un système de gestion de bases de données distribué doit choisir entre deux des trois propriétés suivantes :
\begin{enumerate}
 \item Cohérence ;
 \item Disponibilité ;
 \item Résistance au morcellement.
\end{enumerate}

Les systèmes de stockage de paires ont eu tendance à se focaliser sur la haute disponibilité et la forte tolérance aux pannes. Néanmoins cette tendance ne signifie pas pour autant que la cohérence n'a pas été pensée.
En réalité les systèmes d'aujourd'hui laissent la possibilité de configurer un compromis entre cohérence et performances. Par exemple, les niveaux d'isolation des transactions permettent
d'augmenter la sureté des données, en gérant des problèmes comme les \textit{phantom reads}, mais nécessitent une perte de performances dues aux vérifications induites.
\newline
De la même manière, MongoDB, un système NoSQL orienté document, correspond au type de système qui nous intéresse. Celui-ci propose différents niveaux de cohérence lors de chaque écriture réalisée par le client, et délègue donc 
la cohérence des transactions au client. 
\newline
\newline
Il est donc tout à fait possible de traiter des transactions avec un système de stockage de paires, le tout est d'être conscient du compromis à faire entre les performances et la cohérence des données.



\section{Partie 2 : Transactions et équilibrage de charge}
\subsection{Que pouvez-vous dire de l'augmentation du temps de réponse en fonction de C ?}

Les résultats suivants présentent le temps d'exécution moyen des transactions lorsque l'on fait varier C, avec un seul store. Ces temps ne prennent pas en compte
les communications réseaux ou un code arbitraire du processus, mais uniquement le temps d'exécution de la transaction.

\begin{figure}[h]
   \centering
   \includegraphics[scale=0.5]{images/one_store.png}
   \caption{Temps d'exécution moyen des transactions en fonction du nombre d'applications en parallèle}
\end{figure}

Nous pouvons observer trois choses sur les résultats obtenus. Les courbes sont décroissantes asymptotiques sur l'axe des ordonnées, le temps de réponse moyen 
est plus élevé lorsque C diminue et les variations de temps entre transactions sont plus élevés lorsque C diminue.
\newline
\newline
Les variations de temps s'expliquent par le lissage réalisé par le calcul des moyennes. Lorsque C augmente, le nombre de valeurs à moyenner augmente aussi
renforçant la tendance à des valeurs communes et donc à plus lisser la courbe. Les problèmes propres au système d'exploitation (commutation de contexte, ...) 
se retrouvent amoindris lorsque C augmente, tandis qu'ils peuvent faire varier de manière significative le temps de réponse de deux transactions consécutives.
\newline
\newline
La décroissance asymptotique et le fait que le temps soit plus élevé lorsque C diminue peuvent s'expliquer par divers mécanismes internes à KVStore. 
Les benchmarks ont été réalisés avec la configuration standard de KVStore. Par exemple, KVStore possède un cache interne, \textit{Berkeley DB Java Edition}, mal dimensionné pour nos tests, entraînant 
une augmentation de sa taille au fur et à mesure du temps, augmentant alors le temps des transactions. Plus le nombre de transactions est important, plus la taille du cache tend vers 
une valeur convenable pour nos benchmarks. Donc plus C est petit, plus le dimensionnement du cache met du temps. Ceci pourrait être l'une des raisons
expliquant cette différence de temps qui devrait logiquement être inverse.

\subsection{Le temps de réponse est-il identique sur S1 et S2 ? Obtient-on un temps de réponse proche du cas C=5 de la question précédente ?}

Les résultats suivants présentent le temps d'exécution moyen des transactions lorsque l'on fait varier C, avec deux stores.

\begin{figure}[h]
   \centering
   \includegraphics[scale=0.5]{images/two_store.png}
   \caption{Temps d'exécution moyen des transactions en fonction du nombre d'applications en parallèle}
\end{figure}

Nous constatons ici que les résultats obtenus sont très proches de ceux avec un seul store, malgré un temps de réponse plus long au début. Le temps de réponse entre S1 et S2 est identique.
Le temps de réponse du cas C=5 est proche à partir de la 200\up{ème} transaction, donc le numéro de transaction 40. 

\subsection{Décrire toutes les informations à connaître pour pouvoir déplacer une donnée de $S_{i}$ vers $S_{j}$}

Afin de pouvoir déplacer une donnée de $S_{i}$ vers $S_{j}$, nous devons connaître :
\begin{enumerate}
 \item Les adresses IP et ports de $S_{i}$ et $S_{j}$. Ceux-ci sont connus par le moniteur ;
 \item Les clés à migrer de $S_{i}$ vers $S_{j}$.
\end{enumerate}


\subsection{Proposer une solution pour déplacer une ou plusieurs données depuis un store trop chargé vers un store moins chargé}

Nous avons répondu à cette question dans la section 2.5.

\newpage
\part{Conclusion}

La solution proposée ici dépasse de loin ce qui était requis pour le projet. A travers cette matière du master SAR, nous avons pu réaliser une véritable application répartie
permettant l'exécution de transaction KVStore de manière transparente pour l'utilisateur. D'une certaine manière, nous avons rendu KVStore réparti. En plus d'avoir
une solution fonctionnelle, répondant pleinement au sujet proposé, nous avons ajouté les fonctionnalités suivantes :
\begin{enumerate}
 \item Tolérance aux pannes ;
 \item Transactions libres. Le client n'a qu'à définir des transactions et les soumettre au système, plutôt qu'à écrire tout le système lui-même ;
 \item Migration de données intelligente. L'algorithme décrit précédemment permet de sélectionner efficacement le meilleur noeud sur lequel migrer les données ;
 \item Transactions multi-clés ;
 \item Hétérogénéité des échanges à travers le protocole HTTP.
\end{enumerate}

Finalement, ce projet nous aura permis de construire ce que nous étudions en cours depuis le début du master.

\end{document}
